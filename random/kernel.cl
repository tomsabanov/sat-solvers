
__kernel void brute(__global const int *clauses,
						 __global int *solution,		
						 int max_var,				
						 int cl_count,
						int var_count)						
{																				
	int id = get_global_id(0);	
	int seed = 27 + id;
	if(id==0)
		solution[0] = 0;


	int iter = 0;
	float st = (float)id/get_global_size(0)*var_count;
	int izbranih = (int)st;
	while(iter<4000){
		if(iter%20==0){
			if(solution[0]!=0)
				break;
		}
		int bits[128];
		//flipnemo izbrane
		int k = 0;
		for(int i = 0;i<izbranih && k < 500;){
			seed = (seed * 0x5DEECE66DL + 0xBL) & ((1L << 48) - 1);
			int result = seed >> 16;
			result = abs(result);
			result = result%var_count;
			if(bits[result]==0){
				bits[result]=1;
				i++;
			}
			k++;
		}
		
		//check loop
		int najdu = 1;
		for(int i = 0;i<cl_count;i++){
			int rez = 0;
			for(int j = 0;j<max_var;j++){
				int ele = clauses[i*max_var + j];
				if(ele == 0){
					break;
				}
				if(ele<0){
					ele = ele*(-1);
					rez = rez | !bits[ele-1];
				}else{
					rez = rez | bits[ele-1];
				}
			}
			if(rez==0){
				najdu = 0;
				break;
			}
		}
		if(najdu){
			solution[0] = id;
			for(int i = 0;i<var_count;i++)
				printf("%d ",bits[i]);
			printf("\n");
			break;
		}


		iter++;
	}
	
}