#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include "Parser.h"

using namespace std;

int main(void) {
	char* input_file = "test.cnf";
	int* clauses = NULL;

	printf("Parsing input file.\n");
	Parser p;
	p.parse(input_file);
	clauses = p.getTable();
	int cl_count = p.getClausesCount();
	int var_count = p.getVariableCount();
	int max_var = p.getMaxVariables();

	for (int i = 0;i < cl_count;i++) {
		for (int j = 0;j < max_var;j++)
			printf("%d ", clauses[i*max_var + j]);
		printf("\n");
	}
}