#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

class Parser {
	int variables_num, clauses_num,max_var;
	int* parsedTable;
public:
	void parse(char* file);
	int getClausesCount();
	int getVariableCount();
	int getMaxVariables();
	int* getTable();
};

//gets max number of variables in any clause
int Parser::getMaxVariables() {
	return max_var;
}

//gets how many clauses in SAT problem there are
int Parser::getClausesCount() {
	return clauses_num;
}
//gets how many different variables there are
int Parser::getVariableCount() {
	return variables_num;
}
//gets all clauses as a 1d table
int* Parser::getTable() {
	return parsedTable;
}

void Parser::parse(char* input) {
	std::ifstream file(input);
	std::string str;
	if (!file.is_open()) {
		printf("Could not open file!\n");
		system("exit");
	}
	//first pass - get max number of variables in one clause
	int claus = -1;
	int maxNumbers = 0;
	while (std::getline(file, str)) {

		char* str2 = &str[0];
		char *token = strtok(str2, " ");
		if (strcmp("c", token) == 0)
			continue;
		if (strcmp("%", token) == 0)
			break;
		//skip first row
		if (claus != -1) {
			int numbers = 0;
			while (token != NULL)
			{
				numbers++;
				token = strtok(NULL, " ");
			}
			if (numbers > maxNumbers)
				maxNumbers = numbers;
		}
		claus++;
	}
	clauses_num = claus;
	//sub 0 
	maxNumbers--;
	max_var = maxNumbers;
	int* clauses = (int*)calloc(claus*maxNumbers, sizeof(int));
	//second pass
	file.clear();
	file.seekg(0, std::ios::beg);
	int row = -1;
	while (std::getline(file, str)) {

		char* str2 = &str[0];
		char *token = strtok(str2, " ");
		if (strcmp("c", token) == 0)
			continue;
		if (strcmp("%", token) == 0)
			break;
		//first row
		if (row == -1) {
			token = strtok(NULL, " ");
			token = strtok(NULL, " ");
			int vrednost;
			stringstream s(token);
			s >> vrednost;
			variables_num = vrednost;
		}
		else {
			int index = 0;
			while (token != NULL)
			{
				int vrednost;
				stringstream s(token);
				s >> vrednost;
				if (vrednost != 0)
					clauses[row*maxNumbers + index] = vrednost;
				index++;
				token = strtok(NULL, " ");
			}
		}
		row++;
	}
	parsedTable = clauses;
}

