#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include "Parser.h"
#include <CL/cl.h>

#define SIZE			(1024)
#define WORKGROUP_SIZE	(256)
#define MAX_SOURCE_SIZE	16384

using namespace std;

void intToBits(int array[], int stevilo, int bits) {
	for (int i = 0; i < bits; i++) {
		array[i] = (stevilo >> i) & 1;
	}
}
void testSolution(int* clauses,int array[], int cl_count, int max_var) {
	int najdu = 1;
	for (int i = 0;i < cl_count;i++) {
		int rez = 0;
		for (int j = 0;j < max_var;j++) {
			int ele = clauses[i*max_var + j];
			if (ele == 0) {
				break;
			}
			if (ele < 0) {
				ele = ele * (-1);
				rez = rez | !array[ele - 1];
			}
			else {
				rez = rez | array[ele - 1];
			}
		}
		if (rez == 0) {
			najdu = 0;
			break;
		}
	}
	if (najdu)
		printf("Uredu resitev\n");
	else
		printf("Ni uredu resitev\n");
}

int main(void) {
	char* input_file = "test2.cnf";
	int* clauses = NULL;

	printf("Parsing input file.\n");
	Parser p;
	p.parse(input_file);
	clauses = p.getTable();
	int cl_count = p.getClausesCount();
	int var_count = p.getVariableCount();
	int max_var = p.getMaxVariables();

	//izpis
	/*for (int i = 0;i < cl_count;i++) {
		for (int j = 0;j < max_var;j++)
			printf("%d ", clauses[i*max_var + j]);
		printf("\n");
	}*/

	char ch;
	int i;
	cl_int ret;

	FILE *fp;
	char *source_str;
	size_t source_size;
	int wkSize = WORKGROUP_SIZE;
	fp = fopen("kernel.cl", "r");
	if (!fp)
	{
		fprintf(stderr, ":-(#\n");
		exit(1);
	}
	source_str = (char*)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	source_str[source_size] = '\0';
	fclose(fp);

	// Podatki o platformi
	cl_platform_id	platform_id[10];
	cl_uint			ret_num_platforms;
	char			*buf;
	size_t			buf_len;
	ret = clGetPlatformIDs(10, platform_id, &ret_num_platforms);
	// max. "stevilo platform, kazalec na platforme, dejansko "stevilo platform

// Podatki o napravi
	cl_device_id	device_id[10];
	cl_uint			ret_num_devices;
	// Delali bomo s platform_id[0] na GPU
	ret = clGetDeviceIDs(platform_id[0], CL_DEVICE_TYPE_GPU, 10,
		device_id, &ret_num_devices);
	// izbrana platforma, tip naprave, koliko naprav nas zanima
	// kazalec na naprave, dejansko "stevilo naprav

// Kontekst
	cl_context context = clCreateContext(NULL, 1, &device_id[0], NULL, NULL, &ret);
	// kontekst: vklju"cene platforme - NULL je privzeta, "stevilo naprav, 
	// kazalci na naprave, kazalec na call-back funkcijo v primeru napake
	// dodatni parametri funkcije, "stevilka napake

// Ukazna vrsta
	cl_command_queue command_queue = clCreateCommandQueue(context, device_id[0], 0, &ret);
	// kontekst, naprava, INORDER/OUTOFORDER, napake

// Delitev dela
	size_t local_item_size = WORKGROUP_SIZE;
	size_t num_groups = ((cl_count*max_var - 1) / local_item_size + 1);
	size_t global_item_size = num_groups * local_item_size;


	int* solution = (int*)malloc(sizeof(int)*1);
	cl_mem clauseMemObject = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		cl_count*max_var * sizeof(int), clauses, &ret);
	cl_mem solutionMemObject = clCreateBuffer(context, CL_MEM_READ_WRITE,
		1 * sizeof(int), NULL, &ret);
	
	// Priprava programa
	cl_program program = clCreateProgramWithSource(context, 1, (const char **)&source_str,
		NULL, &ret);
	// kontekst, "stevilo kazalcev na kodo, kazalci na kodo,		
	// stringi so NULL terminated, napaka													

// Prevajanje
	ret = clBuildProgram(program, 1, &device_id[0], NULL, NULL, NULL);
	// program, "stevilo naprav, lista naprav, opcije pri prevajanju,
	// kazalec na funkcijo, uporabni"ski argumenti

// Log
	size_t build_log_len;
	char *build_log;
	ret = clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG,
		0, NULL, &build_log_len);
	// program, "naprava, tip izpisa, 
	// maksimalna dol"zina niza, kazalec na niz, dejanska dol"zina niza
	build_log = (char *)malloc(sizeof(char)*(build_log_len + 1));
	ret = clGetProgramBuildInfo(program, device_id[0], CL_PROGRAM_BUILD_LOG,
		build_log_len, build_log, NULL);
	printf("%s\n", build_log);
	free(build_log);

	// "s"cepec: priprava objekta
	cl_kernel kernel = clCreateKernel(program, "brute", &ret);
	// program, ime "s"cepca, napaka

	size_t buf_size_t;
	clGetKernelWorkGroupInfo(kernel, device_id[0], CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(buf_size_t), &buf_size_t, NULL);
	printf("veckratnik niti = %d", buf_size_t);

	scanf("%c", &ch);

	// "s"cepec: argumenti
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&clauseMemObject);
	ret |= clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&solutionMemObject);
	ret |= clSetKernelArg(kernel, 2, sizeof(cl_int), (void *)&max_var);
	ret |= clSetKernelArg(kernel, 3, sizeof(cl_int), (void *)&cl_count);
	ret |= clSetKernelArg(kernel, 4, sizeof(cl_int), (void *)&var_count);
	// "s"cepec, "stevilka argumenta, velikost podatkov, kazalec na podatke


	ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL,
		&global_item_size, &local_item_size, 0, NULL, NULL);
	// vrsta, "s"cepec, dimenzionalnost, mora biti NULL, 
	// kazalec na "stevilo vseh niti, kazalec na lokalno "stevilo niti, 
	// dogodki, ki se morajo zgoditi pred klicem

// Kopiranje rezultatov
	ret = clEnqueueReadBuffer(command_queue, solutionMemObject, CL_TRUE, 0,
		1 * sizeof(int), solution, 0, NULL, NULL);
	// branje v pomnilnik iz naparave, 0 = offset
	// zadnji trije - dogodki, ki se morajo zgoditi prej

	printf("Solution as an int: %d\n", solution[0]);

	int array[32];
	intToBits(array, solution[0], var_count);
	testSolution(clauses,array,cl_count,max_var);

	// "ci"s"cenje
	ret = clFlush(command_queue);
	ret = clFinish(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);
	
	system("pause");
}