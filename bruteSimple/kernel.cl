
void intToBits(int array[],int stevilo,int bits){
	for (int i = 0; i < bits; i++) {
		array[i] = (stevilo >> i) & 1;
	}
}

__kernel void brute(__global const int *clauses,
						 __global int *solution,		
						 int max_var,				
						 int cl_count,
						int var_count)						
{																				
	int id = get_global_id(0);	
	int velikost = (int)pow(2.0f,(float)var_count);

	//set starting solution
	if(id == 0)
		solution[0] = 0;

	int iter = 0;

	while(id<velikost){
		if(iter == 30){
			iter = 0;
			if(solution[0]!=0)
				break;
		}
		int bits[32];
		intToBits(bits,id,var_count);
		/*for(int i = 0;i<32;i++){
			printf("%d ",bits[i]);
		}
		printf("\n");*/
		int najdu = 1;
		for(int i = 0;i<cl_count;i++){
			int rez = 0;
			for(int j = 0;j<max_var;j++){
				int ele = clauses[i*max_var + j];
				if(ele == 0){
					break;
				}
				if(ele<0){
					ele = ele*(-1);
					rez = rez | !bits[ele-1];
				}else{
					rez = rez | bits[ele-1];
				}
			}
			if(rez==0){
				najdu = 0;
				break;
			}
		}
		if(najdu){
			solution[0] = id;
			break;
		}

		iter++;
		id+=get_global_size(0);
	}	
}